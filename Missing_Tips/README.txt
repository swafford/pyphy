mismatch.py v1.0

DEVELOPER:
Andrew Swafford
Andrew.swafford@lifesci.ucsb.edu
Developed for Python 2.7 under Windows 7.

PURPOSE:
Compare any two trees and report taxa that do not appear on the first tree.

USAGE:
./mismatch.py -t1 [Path to newick tree] -t2 [Path to newick tree]

OUTPUTS:
A list, separated by newlines is printed to screen. It is reccomended to pipe stdout to a text file to save work.

SAMPLE DATA:
Taxa A has been removed from tree2
    COMMAND:
	    ./mismatch.py -t1 tree1.tree -t2 tree2.tree
	OUTPUT:
	    A