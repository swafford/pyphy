#! /usr/bin/env python

from ete3 import Tree
import argparse
import re

def match(tree1,tree2):
    with open(tree1,'r') as tree:
        for line in tree:
            tree = clean(line)
            break
    tree1_tree = Tree(tree)
    tree2_tree = Tree(tree2)
    tree1_names = [x.name for x in tree1_tree.get_leaves()]
    tree2_names = [x.name for x in tree2_tree.get_leaves()]
    missing = list(set(tree1_names)-set(tree2_names))
    missing.sort()
    for name in missing:
        print(name)

def clean(newick):
    banned = ['=',';']
    for i in banned:
        newick = re.sub(i,"_",newick)
    tree = list(newick)
    if tree[-1] == "_": tree[-1] = ";"
    if tree[-2] == '_': tree[-2] = ";"
    tree = "".join(tree)
    return(tree)
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = "find OTUs that are on the first tree but do not appear on the second.")
    parser.add_argument('-t1', type = str, required = True,
                        help = "Path to first tree")
    parser.add_argument('-t2', type = str, required = True,
                        help = "Path to second tree")
    args = parser.parse_args()
    match(args.t1,args.t2)
