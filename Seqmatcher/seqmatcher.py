import re
from Bio import SeqIO
import argparse
import os

def main(seqNew,seqOld,output):
    seqN = SeqIO.to_dict(SeqIO.parse(seqNew,'fasta'))
    accpull = re.compile('(.*?)_gi',re.IGNORECASE)
    accpull2 =  re.compile('([0-9]{7,}).*?_',re.IGNORECASE)
    with open(seqOld,'r') as mask:
        seqO = mask.read().splitlines()
    for name in seqN.keys():
        match = re.search(accpull,name)
        if match is not None and match.group(1):
            lame = match.group(1).split("_")[-1].split('.')[0]
            if len(lame) > 4:
                x = [k for k in seqO if lame in k]
        else:
            match = re.search(accpull2,name)
            print name
            print match.group(1)
            x = [k for k in seqO if match.group(1) in k]
        if x:
            seqN[x[0]] = seqN.pop(name)
    with open("{0}_repaired.fasta".format(output),'w') as outFile:
        for k,v in seqN.items():
            #if '=' in k: print k
            outFile.write(">%s\n%s\n"%(k,v.seq))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = "Repair damaged tip labels")
    parser.add_argument('-d', metavar = 'damaged', type = str,
                        help = "FASTA with damaged names")
    parser.add_argument('-r', metavar = 'clean', type = str, required = True,
                        help = "FASTA with clean names")
    parser.add_argument('-o', metavar = 'output', type = str, default = '.',
                        help = "Path to directory into which outputs will be saved.")
    args = parser.parse_args()
    seqNew = args.d
    seqOld = args.r
    output = args.o
    main(seqNew,seqOld,output)
