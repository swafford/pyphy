SEQMATCHER v1.0.3

For comments, questions, and bug reports please contact: andrew.swafford@lifesci.ucsb.edu

PURPOSE:
	SEQMATCHER is designed to repair damaged fasta headers from RAxML runs and programs that impose cutoffs.
	
USAGE:
	python seqmatcher.py -d DAMAGEDHEADERS -r FULLHEADERS -o OUTPUT