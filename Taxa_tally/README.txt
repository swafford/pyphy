taxa_tally v1.0.0

Tested in Windows 7 and python 2.7. No guarantees of accurate results on other systems.

Taxa Tally is a simple script that allows you to quickly count the number of higher order taxanomic ranks represented by a set of species.
DEPENDENCIES:
Python 2.7
ETE3	http://etetoolkit.org/download/


INPUTS:
-i	A file with a set of taxa.  Each taxa should be present on a new line, and should at least possess the "Genus species" information.
		This tool will theoretically work with just a genus name or higher taxanomic rank as long as you are not attempting to count ranks lower than the greatest rank placed in the input file.
-r	Taxanomic rank you wish to count. you may choose from the following options:
		superkingdom
		kingdom
		phylum
		superclass
		class
		subclass
		infraclass
		order
		suborder
		infraorder
		superfamily
		family
		subfamily
		tribe
		genus
		subgenus
		species group
		species
		subspecies
-h	Print a semi-helpful version of this document.

OUTPUTS:
Counts as well as percentage of total representation per rank will be printed on the screen.  Piping stdout to a .txt file is recommended to save your work.
If a taxa cannot be placed within the rank requested, it is placed in a "lost list" which is printed above the results upon completion.  In addition, the number
of taxa that have not been included in the results will be clearly printed along with the summary statistics.

SAMPLE DATA:
COMMAND:
python taxa_tally.py -i taxa.txt -r phylum > output.txt

OUTPUT.txt:
LEVEL	COUNT	PERCENT
Arthropoda 	98 	29.878
Placozoa 	1 	0.305
Annelida 	2 	0.61
Priapulida 	1 	0.305
Mollusca 	5 	1.524
Chordata 	207 	63.11
Brachiopoda 	1 	0.305
Hemichordata 	1 	0.305
Cnidaria 	4 	1.22
Platyhelminthes 	7 	2.134
Echinodermata 	1 	0.305
Total # of Levels Represented:	11
Total # of Taxa counted:	328

COMMAND:
python taxa_tally.py -i taxa.txt -r class > output.txt

OUTPUT.txt:
You will need to manually count the following taxa:
['Branchiostoma floridae', 'Ophiophagus hannah', 'Petromyzon marinus', 'Chelonia mydas', 'Python bivittatus', 'Thamnophis sirtalis', 'Pelodiscus sinensis', 'Latimeria chalumnae', 'Chrysemys picta bellii', 'Gekko japonicus', 'Alligator mississippiensis', 'Branchiostoma belcheri', 'Protobothrops mucrosquamatus', 'Anolis carolinensis', 'Schmidtea mediterranea', 'Alligator sinensis', 'Priapulus caudatus', 'Trichoplax adhaerens'] 

LEVEL	COUNT	PERCENT
Branchiopoda 	2 	0.645
Aves 	61 	19.677
Arachnida 	5 	1.613
Enteropneusta 	1 	0.323
Trematoda 	5 	1.613
Chondrichthyes 	1 	0.323
Actinopteri 	33 	10.645
Bivalvia 	1 	0.323
Merostomata 	1 	0.323
Cephalopoda 	1 	0.323
Gastropoda 	3 	0.968
Amphibia 	1 	0.323
Polychaeta 	1 	0.323
Ascidiacea 	1 	0.323
Hirudinida 	1 	0.323
Anthozoa 	3 	0.968
Hydrozoa 	1 	0.323
Mammalia 	95 	30.645
Cestoda 	1 	0.323
Echinoidea 	1 	0.323
Lingulata 	1 	0.323
Insecta 	90 	29.032
Total # of Levels Represented:	22
Total # of Taxa counted:	310
Total # of Taxa NOT counted:	18
