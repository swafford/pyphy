#!/usr/bin/env python

#Created by Andrew Swafford.
#Direct comments or questions to andrew.swafford@lifesci.ucsb.edu
import ete3
from ete3 import NCBITaxa
import csv
import argparse
from decimal import *
ncbi = NCBITaxa()

def taxDump(taxaFile,tLevel):
    lCount = {}
    finalCount = {}
    taxaLabels = {}
    lostTaxa = []
    inTaxa = []
    with open(taxaFile,'rb') as tFile:
        reader = csv.reader(tFile)
        tlist = [x[0] for x in reader]
    print 'counting: ',len(list(set(tlist)))
    tIdDict = ncbi.get_name_translator(tlist)
    print 'Why cant I hold all these taxa?!? ', set(tlist) - set([str(x) for x in tIdDict.keys()])
    #tIdList = [tIdDict[x][0] for x in tlist]
    for taxaName in tIdDict.keys():
        tLineage = ncbi.get_lineage(tIdDict[taxaName][0])
        tRank = ncbi.get_rank(tLineage)
        for key,rank in tRank.items():
            if tLevel == str(rank):
                lCount.setdefault(key,0)
                lCount[key] = lCount[key]+1
                inTaxa.append(taxaName)
                break
        if len(set(lCount.keys()).intersection(tRank.keys())) == 0:
            lostTaxa.append(taxaName)
    lNames = ncbi.get_taxid_translator(lCount.keys())
    tval = 0
    for ID,name in lNames.items():
        finalCount.setdefault(name,lCount[ID])
        tval = tval+lCount[ID]
    printout(finalCount,tval,lostTaxa)

def printout(finalCount,tval,lostTaxa):
    if len(lostTaxa) > 0:
        print "You will need to manually count the following taxa:\n",lostTaxa,"\n"
    if len(finalCount.keys()) > 0:
        i = 0
        print "LEVEL\tCOUNT\tPERCENT"
        sortedKeys = finalCount.keys()
        sortedKeys.sort()
        for key in sortedKeys:
            value = finalCount[key]
            percentRep = round((Decimal(value)/Decimal(tval))*100,3)
            print str(key),"\t",value,"\t",percentRep
            i+=1
        print "Total # of Levels Represented:\t",i
        print "Total # of Taxa counted:\t",tval
        if len(lostTaxa) > 0:
            print "Total # of Taxa NOT counted:\t",len(lostTaxa)
    return(finalCount)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = "Count representation of larger taxanomic ranks")
    parser.add_argument("-i", help='Path to file with Taxa names in the format "Genus species"',
                        required = True)
    parser.add_argument("-r", help='File with each taxa separated by newline',
                        required = True, choices=["superkingdom",
                                                   "kingdom",
                                                   "phylum",
                                                   "superclass",
                                                   "class",
                                                   "subclass",
                                                   "infraclass",
                                                   "order",
                                                   "suborder",
                                                   "infraorder",
                                                   "superfamily",
                                                   "family",
                                                   "subfamily",
                                                   "tribe",
                                                   "genus",
                                                   "subgenus",
                                                   "species group",
                                                   "species",
                                                   "subspecies"])
    args = parser.parse_args()
    taxDump(args.i,args.r)
