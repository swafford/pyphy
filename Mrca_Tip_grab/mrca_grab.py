from ete3 import Tree
import argparse

def get_mrca(tips,tree,formt,output):
    try:
        eteTree = Tree(tree,format=formt)
    except:
        raise ValueError("Either your tree is not proper newick format or you have selected an incorrect format number")
    tuples = tips.split(",")
    print tuples
    if len(tuples) > 2:
        raise ValueError("Your tips must not contain commas except for a single comma separating two tips")
    with open('{0}.txt'.format(output),'w') as outfile:
        node = eteTree.get_common_ancestor([tuples[0],tuples[1]])
        for i in  node.get_leaves():
            outfile.write("{0}\n".format(i.name))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = "Repair damaged tip labels")
    parser.add_argument('-tip', metavar = 'tips', type = str, required = True,
                        help = "Two tip names, separated by a comma")
    parser.add_argument('-tre', metavar = 'tree', type = str, required = True,
                        help = "Tree in newick format")
    parser.add_argument('-f', metavar = 'format', default = 1, type = int,
                        help = "format of newick tree")
    parser.add_argument('-o', metavar = 'output', type = str, default = '.', required = True,
                        help = "output")
    args = parser.parse_args()
    get_mrca(args.tip,args.tre,args.f,args.o)
