MRCA_GRAB v 1.0.0
Last Update: Andrew Swafford 10/8/2016

PURPOSE: MRCA_GRAB is a single command line function designed to get all tips that share a most recent common ancestor with two tips.

USAGE:
python mrca_grab.py -tip TIP1,TIP2 -tre TREEFILE.NWK -o OUTPUTFILE <-f FORMAT>

-tip	Two tips that can be found on the tree designated in '-tre'
-tre	A tree file in newick format
-o		A name/path for the output file
-f		OPTIONAL: change the format ETE3 expects the tree to be in. Default is 1.

DEPENDENCIES:
ETE3
