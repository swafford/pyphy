from ete2 import *
import csv
import argparse
import random
import numpy as np
import sys
import itertools
import pandas as pd
import argparse

def tree_handle(tree,K,outfile_name,targets=None):
    tree = Tree(tree)
    tipList = tree.get_leaves()
    i = 0
    while i < len(tipList):
        tipList[i] = tipList[i].name
        i+=1
    if targets != None:
        df = pd.DataFrame.from_csv(targets, index_col=None,header=None)
        combn = itertools.combinations(df.iloc[:,0],K)
    else:
        combn = itertools.combinations(tipList,K)
    pdValue,nameValue = pdCalc(list(combn),tree,True)
    pdDf = pd.DataFrame(columns = range(K))
    nameArray = np.array(nameValue)
    for i in range(K):
        pdDf[i] = nameArray[:,i]
    pdDf[K] = pdValue
    pdDf.to_csv("{0}_outfile.csv".format(outfile_name), index=False,sep=',')
        
    

def pdCalc(combn,tree,includeRoot):
    x=0
    pd_list = []
    cohort_list = []
    for cohort in combn: #test the PD for each bootstrap replicate
        pd_value = 0
        taxalist = list(cohort)
        mrca = tree.get_common_ancestor(taxalist)
        path = []
        for leaf in taxalist:
            node = tree&leaf
            while node.up:
                if node not in path:
                    path.append(node)
                    pd_value = node.dist + pd_value
                node = node.up
                if node == mrca:
                    break
        if includeRoot == True:
            while node.up:
                if node not in path:
                    path.append(node)
                    pd_value = node.dist + pd_value
                node = node.up
        pd_list.append(pd_value)
        cohort_list.append(taxalist)
        x += 1
        if x%100000 == 0:
            print x
    else:
        return(pd_list,cohort_list)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = "Calculate & bootstrap PDs")
    parser.add_argument("-g", help='File with Taxon names matching tree. Each taxa on a new line.',
                        required = True)
    parser.add_argument("-t", help='Tree file in newick format.',
                        required = True)
    parser.add_argument("-c", help = "Number of taxa to subsample from the taxon set",
                        required = True, type=int)
    parser.add_argument("-o", help = "Output file prefix",
                        required = True)
    args = parser.parse_args()
    tree_handle(args.t,args.c,args.o,args.g)
