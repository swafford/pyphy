PYPHY v1.0.2
Created & Curated by
Andrew Swafford:
	Oakley lab
	Dept. of Ecology, Evolution and Marine Bio.
	Univ. of California, Sanata Barbara.

PURPOSE:
	Pyphy is a collection of small scripts that I have created to help manage and manipulate large-scale phylogenetic data. All scripts are programmed in
	python or R, and will list dependencies in individual READMEs specific to the script.  Typical dependencies are: BioPython, ETE3, and numpy.
	
USAGE:
	Usage instructions will accompany each script in the individual READMEs.  If no README is present, please check the .py file for instructions and user inputs.
	Particular scripts have not been enabled on the command line, and thus require manual alterations in the .py file. These scripts are noted by the #HARDCODE in the
	title of the directory.
	
CONTACT:
	For requests, bug reports, or questions - email andrew.swafford@lifesci.ucsb.edu

DETAILS:

MRCA_GRAB 	Fetches all tips that share a most recent common ancestor with two tips.
TAXA_TALLY	Fetches the number of taxa in a list that represent a user defined taxanomic level.
SUPERCUTS	Removes sequeneces from an alignment that do not appear on a tree.
PD_tools	Tools for calculating phylogenetic distances in python.
Missing_Tips	Reports tips that are missing between two trees.
Seqmatcher	Repairs fasta headers from RAxML runs.